<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Merengue Merengue | Postres caseros a domicilio desde tu smartphone</title>
    
    <?php include('cabeceras.php'); ?>

  </head>

  <body class="postres">

    <header>
      <div class="container">
        <p>POSTRES <span class="glyphicon glyphicon-log-out pull-right logout" aria-hidden="true"></span></p>
        <?php include('header.php'); ?>

    <div id="contenido">
      <div class="container">
        <div class="col-xs-offset-1 col-xs-10">
          <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
            	<span class="no-hay">Si no tienes postres, súbelos es en <a href="/sube-tus-postres/">merenguemerengue.com/sube-tus-postres/</a>.</span>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
          <!-- <img src="resources/img/img_postre.png" class="img-responsive" alt="">
          
          Pastel de chocolate
          
          Delicioso pastel de triple chocolate relleno de crema respostera y frutos rojos.

          AGREGAR A PEDIDOD -->
        </div>
      </div>
    </div>

    <?php include('footer.php'); ?>
    <script>
      $( ".m_postres" ).addClass( "active" );
      var sizes = ["","Personal (1-2 personas)", "Pequeño (3-4 personas)","Mediano (5-10 personas)","Grande (Más de 10 personas)"]
      var misPostres = {};
      var active = 1;
      ref.child("Usuario/"+ref.getAuth().uid+"/productos").on("child_added", function(postkey) {
      	$(".no-hay").remove();
        ref.child("Articulo/"+postkey.key()).on("value", function(elpostre) {
            misPostres[elpostre.key()] = elpostre.val();
            var postre = elpostre.val();
            if(postre) {
              postre.key = elpostre.key();
              var clase="item"
              if(active--==1){
              	clase = "item active"
              }
              var $div = $("<div>", {class: clase, id: postre.key});
              if(postre.hasOwnProperty('imagenes')){
                $div.append($("<img>",{class: "img-responsive", src: "http://res.cloudinary.com/merengue-merengue/image/upload/c_thumb,g_center,w_250,h_250/v1462563303/"+postre.imagenes[0]+".jpg"}));
              }
              var $caption = $("<div>", {class: "carousel-caption"});
              $caption.append($("<p>",{class: "titulo"}).html(postre.nombre));
              $caption.append($("<p>",{class: "descripcion"}).html(postre.descripcion));
              if(postre.hasOwnProperty('precio') && postre.precio.match(/[+-]?\d+(\.\d+)?/g)){
                $caption.append($("<p>",{class: "precio"}).html(Number(postre.precio.match(/[+-]?\d+(\.\d+)?/g)[0])));
              }
              $caption.append($("<div>",{class: "boton_ver inactive"}).append("<a href=''>AGREGAR A PEDIDO</a>"));
              $div.append($caption);
              
              $(".carousel-inner").append($div);
            }
        });        
      });

      ref.child("Usuario/"+ref.getAuth().uid+"/productos").on("child_removed", function(postre) {
        $('#'+postre.key()).remove();
      });
    </script>
    
  </body>

</html>