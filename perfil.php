<!DOCTYPE html>
<html lang="es">
  <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Merengue Merengue | Postres caseros a domicilio desde tu smartphone</title>
    
    <?php include('cabeceras.php'); ?>

  </head>

  <body class="prefil">

    <header>
      <div class="container">
        <p>PERFIL <span class="glyphicon glyphicon-log-out pull-right logout" aria-hidden="true"></span></p>
        <?php include('header.php'); ?>

    <div id="contenido">
      <div class="container">
        <div class="col-xs-offset-1 col-xs-10">
          <img src="resources/img/img_demo.png" class="img-responsive" alt="">
          <div class="boton_ver"><a href="postres.php"><span class="icon-icono1 col-xs-3"></span> <span class="col-xs-9">MIS POSTRES</span></a></div>
          <div class="clearfix"></div>
          <div class="boton_ver inactive"><a href=""><span class="icon-icono2 col-xs-3"></span> <span class="col-xs-9"> PEDIDOS</a></div>
          <div class="clearfix"></div>
          <div class="boton_ver inactive"><a href=""><span class="icon-icono3 col-xs-3"></span> <span class="col-xs-9"> RECORDATORIO DE CUMPLEAÑOS</a></div>
          <div class="clearfix"></div>
          <div class="boton_ver inactive"><a href=""><span class="icon-icono4 col-xs-3"></span> <span class="col-xs-9"> MIS CLIENTES</a></div>
        </div>
      </div>
    </div>

    <?php include('footer.php'); ?>
    <script>
      $( ".m_perfil" ).addClass( "active" );
    </script>

    
  </body>

</html>