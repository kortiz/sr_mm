<!DOCTYPE html>
<html lang="es">
  <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Merengue Merengue | Postres caseros a domicilio desde tu smartphone</title>
    
    <?php include('cabeceras.php'); ?>

  </head>

  <body class="inicio">
    <header>
      <div class="container">
        <p>LOG-IN</p>
        <?php include('header.php'); ?>

    <div id="contenido">
      <div class="container login">
        <div class="col-xs-offset-1 col-xs-10">
          <h3>ENTRAR</h3>
          <div class="boton_log"><a href="" class="login">Inicia sesión con Facebook</a></div>

        </div>
      </div>
    </div>
<script>
    if(ref.getAuth()){
        var userdata = {
            nombre: ref.getAuth().facebook.displayName,
            email: ref.getAuth().facebook.email,
            profileimg: ref.getAuth().facebook.profileImageURL,
            mySQLid: parse("id")
        };
        if(userdata.mySQLid=="Not found"){
        	delete userdata.mySQLid;
        }
        ref.child("Usuario/"+ref.getAuth().uid).update(userdata,function(error){
            if (error) {
                alert("Error!, Manda un mail a john@merenguemerengue.com con esta info: "+error);
            }
            else {
                window.location.href = "perfil.php";
            }
        });
    }
    
    $(".login").click(function(){
    ref.authWithOAuthPopup("facebook", function(error, authData) {
      if (error) {
        if (error.code === "TRANSPORT_UNAVAILABLE") {
          // fall-back to browser redirects, and pick up the session
          // automatically when we come back to the origin page
          $(".login").html("Autenticando con Facebook. Por favor espera.");
          ref.authWithOAuthRedirect("facebook", function(error) { /* ... */ });
        }
        else{
          console.log("Login Failed!", error);
          alert("Error, vuelve a intentar");
        }
      } else {
        console.log("Authenticated successfully with payload:", authData);
       
        var userdata = {
            nombre: ref.getAuth().facebook.displayName,
            email: ref.getAuth().facebook.email,
            profileimg: ref.getAuth().facebook.profileImageURL,
            mySQLid: parse("id")
        };
        if(userdata.mySQLid=="Not found"){
        	delete userdata.mySQLid;
        }
        console.log(userdata);

        if(typeof(userdata.email) == "undefined"){
          delete(userdata.email);
        }
        ref.child("Usuario/"+ref.getAuth().uid).update(userdata,function(error){
            if (error) {
                alert("Error!, Manda un mail a john@merenguemerengue.com con esta info: "+error);
            }
            else {
                window.location.href = "perfil.php";
            }
        });
      }
    });
    });
    function parse(val) {
    var result = "Not found",
        tmp = [];
    location.search
    //.replace ( "?", "" ) 
    // this is better, there might be a question mark inside
    .substr(1)
        .split("&")
        .forEach(function (item) {
        tmp = item.split("=");
        if (tmp[0] === val) result = decodeURIComponent(tmp[1]);
    });
    return result;
    }
    </script>
    <?php include('footer.php'); ?>
    
  </body>
</html>